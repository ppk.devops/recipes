#!/usr/bin/env bash

apt update && apt install -y curl wget build-essential sudo
curl -s https://packagecloud.io/install/repositories/sensu/stable/script.deb.sh | bash
curl -s https://packagecloud.io/install/repositories/sensu/community/script.deb.sh | bash
sed -i 's/debian/ubuntu/g' /etc/apt/sources.list.d/sensu_community.list
sed -i 's/buster/bionic/g' /etc/apt/sources.list.d/sensu_community.list
apt update && apt install sensu-go-backend sensu-go-agent sensu-go-cli sensu-plugins-ruby
/opt/sensu-plugins-ruby/embedded/bin/gem install vmstat
mkdir handler && cd handler
wget https://github.com/sensu/sensu-slack-handler/releases/download/1.3.2/sensu-slack-handler_1.3.2_linux_amd64.tar.gz
tar xvf sensu-slack-handler_1.3.2_linux_amd64.tar.gz
cp bin/* /bin && cd .. && rm -r handler
sensu-install -P sensu-plugins-disk-checks,sensu-plugins-http,sensu-plugins-cpu-checks,sensu-plugins-network-checks,sensu-plugins-memory-checks,sensu-plugins-load-checks,sensu-plugins-io-checks
echo "sensu   ALL=(ALL:ALL) NOPASSWD: ALL" >>  /etc/sudoers.d/sensu
chmod 0440 /etc/sudoers.d/sensu
chown root.root /etc/sudoers.d/sensu
