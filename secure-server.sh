#!/bin/env bash

echo '[*] Updating repositories...'

apt-get update
apt-get install -y vim

echo '[*] Securing SSH...'

sed -i \
-e '/#Port 22/s/^#//' \
-e '/#PubkeyAuthentication yes/s/^#//' \
-e '/#StrictModes yes/s/^#//' \
-e 's/#MaxAuthTries 6/MaxAuthTries 1/' \
-e 's/PermitRootLogin yes/PermitRootLogin prohibit-password/' \
-e 's/#PasswordAuthentication yes/#PasswordAuthentication no/' \
-e '/#PasswordAuthentication no/s/^#//' \
-e '/#PermitEmptyPasswords no/s/^#//' \
-e '/X11Forwarding yes/s/^/#/' \
/etc/ssh/sshd_config
systemctl reload ssh

echo 'Done'

echo '[*] Installing fail2ban...'

apt-get install -y fail2ban
sed -i \
-e 's/bantime  = 10m/bantime = 6000m/' \
-e 's/findtime  = 10m/findtime = 60m/' \
-e 's/maxretry = 5/maxretry = 2/' \
/etc/fail2ban/jail.conf
systemctl reload fail2ban

echo 'Done'

echo '[*] Installing shorewall...'

apt-get install -y shorewall
cd /etc/shorewall
cp -v /usr/share/doc/shorewall/examples/one-interface/* .
mv shorewall.conf original-shorewall.conf
gzip -d shorewall.conf.gz

sed -i \
-e 's/STARTUP_ENABLED=No/STARTUP_ENABLED=Yes/' \
/etc/shorewall/shorewall.conf

sed -i \
-e 's/NET_IF/eth0/' \
-e 's/dhcp,//' \
/etc/shorewall/interfaces

sed -i 's/startup=0/startup=1/' /etc/default/shorewall

sed -i \
-e '/^\?SECTION NEW/a\' \
-e 'ACCEPT net \$FW tcp 22 \
ACCEPT net \$FW tcp 80 \
ACCEPT net \$FW tcp 443' \
/etc/shorewall/rules

systemctl enable --now shorewall

echo '[*]----> Done'
